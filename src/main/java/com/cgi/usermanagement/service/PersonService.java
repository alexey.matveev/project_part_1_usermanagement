package com.cgi.usermanagement.service;

import com.cgi.usermanagement.api.GroupDto;
import com.cgi.usermanagement.api.PersonCreateDto;
import com.cgi.usermanagement.api.PersonDto;
import com.cgi.usermanagement.api.PersonWithAddressGroupRoleDto;
import com.cgi.usermanagement.entities.Group;
import com.cgi.usermanagement.entities.Person;
import com.cgi.usermanagement.repository.PersonRepository;
import com.cgi.usermanagement.service.mapping.BeanMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class PersonService {

    private PersonRepository personRepository;
    private BeanMapping beanMapping;

    @Autowired
    public PersonService(PersonRepository personRepository, BeanMapping beanMapping) {
        this.personRepository = personRepository;
        this.beanMapping = beanMapping;
    }

//    //GROUP by ID
//    @Transactional(readOnly = true)
//    public GroupDto findGroupById(int id) {
//        System.out.println("PersonService. Try to find GROUP by this ID:  "+ id);
//        Group group = personRepository.findGroup(id)
//                .orElseThrow(RuntimeException::new);
//        return beanMapping.mapTo(group, GroupDto.class);
//    }


    //by ID
    @Transactional(readOnly = true)
    public PersonDto findPersonById(Long id) {
        System.out.println("PersonService. Try to find Person by this ID:  "+ id);
        Person person = personRepository.findPersonById(id)
                .orElseThrow(RuntimeException::new);
        return beanMapping.mapTo(person, PersonDto.class);
    }

    //by email
    @Transactional(readOnly = true)
    public PersonDto findPersonByEmail(String email) {
        System.out.println("PersonService. Try to find Person by this email:  "+ email);
        Person person = personRepository.findPersonByEmail(email)
                .orElseThrow(RuntimeException::new);
        return beanMapping.mapTo(person, PersonDto.class);
    }

    public PersonDto createPerson(PersonCreateDto personCreateDto) {
        Person person = beanMapping.mapTo(personCreateDto, Person.class);
        Person savedPerson = personRepository.saveAndFlush(person);
        return beanMapping.mapTo(savedPerson, PersonDto.class);
    }


    //by ID with all info
    @Transactional(readOnly = true)
    public PersonWithAddressGroupRoleDto findPersonByIdWithAllInfo(Long id) {
        System.out.println("PersonService. Try to find Person by this ID:  "+ id);
        Person person = personRepository.findPersonByIdWithAllInfo(id)
                .orElseThrow(RuntimeException::new);
        return beanMapping.mapTo(person, PersonWithAddressGroupRoleDto.class);
    }

//    public PersonDto mapToDto(Person person) {
//        PersonDto personDto = new PersonDto();
//        personDto.setIdPerson(person.getIdPerson());
//        personDto.setName(person.getName());
//
//        return personDto;
//    }


}
