//package com.cgi.usermanagement.service;
//
//import com.cgi.usermanagement.api.AddressDto;
//import com.cgi.usermanagement.entities.Address;
//
//import com.cgi.usermanagement.repository.AddressRepository;
//import com.cgi.usermanagement.service.mapping.BeanMapping;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//import java.util.ArrayList;
//import java.util.List;
//
//
//@Service
//@Transactional
//public class AddressService {
//
//    private AddressRepository addressRepository;
//    private BeanMapping beanMapping;
//
//    @Autowired
//    public AddressService(AddressRepository addressRepository, BeanMapping beanMapping) {
//        this.addressRepository = addressRepository;
//        this.beanMapping = beanMapping;
//    }
//
//
//
//    public List<AddressDto> findAddressByPostCode(String postCode) {
//
//        List<Address> addresses = addressRepository.findAddressByPostCode(postCode);
//        List<AddressDto> addressDtos = new ArrayList<>();
//        addresses.forEach(a -> {
//            addressDtos.add(mapToDto(a));
//        });
//
//        return addressDtos;
//    }
//
//    public List<AddressDto> findAddressesByPostCode(String postCode) {
//        return beanMapping.mapTo(addressRepository.findAddressByPostCode(postCode), AddressDto.class);
//    }
//
//
//
//
//    public AddressDto mapToDto(Address address) {
//        AddressDto addressDto = new AddressDto();
//        addressDto.setIdAddress(address.getIdAddress());
//        addressDto.setCity(address.getCity());
//
//        return addressDto;
//    }
//
//    public void deleteById(Long id){
//        addressRepository.deleteById(id);
//    }
//
//
//}
