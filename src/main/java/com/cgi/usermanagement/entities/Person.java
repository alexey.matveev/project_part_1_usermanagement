package com.cgi.usermanagement.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "person")
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_person")
    private Long idPerson;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "email", nullable = false, length = 100)
    private String email;

    @Column(name = "password", nullable = false, length = 255)
    private String password;

    @Column(name = "salary", nullable = false)
    private int salary;


    //Info about 1 to many table address to person + getter setter address
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_address", insertable = false, updatable = false)
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
////////////////////////////////////////

//    @ManyToMany
//    @JoinTable(name = "persons_groups",
//            joinColumns = @JoinColumn(name = "id_person"),
//            inverseJoinColumns = @JoinColumn(name = "id_group"))
//    private Set<Group> groups;
    //GETTER, SETTER for Groups

//    @ManyToMany(mappedBy = "persons",  fetch = FetchType.LAZY)
//    private List<Group> groups = new ArrayList<>();
//    //GETTER, SETTER for Groups

    @ManyToMany //(fetch = FetchType.LAZY)
    @JoinTable(
            name = "persons_groups",
            joinColumns = @JoinColumn(name = "id_person"),
            inverseJoinColumns = @JoinColumn(name = "id_group"))
    @OrderBy("idGroup asc")
    private List<Group> group = new ArrayList<>();

    public List<Group> getGroup() {
        return group;
    }

    public void setGroup(List<Group> group) {
        this.group = group;
    }

    public Person() {
    }

    public Person(Long idPerson, String name, String email, String password, int salary) {
        this.idPerson = idPerson;
        this.name = name;
        this.email = email;
        this.password = password;
        this.salary = salary;
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idPerson=" + idPerson +
                ", namePerson='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }
}


