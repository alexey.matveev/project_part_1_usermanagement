package com.cgi.usermanagement.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "groups")
public class Group implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_group")
    private int idGroup;

    @Column(name = "name_group", nullable = false, length = 100)
    private String nameGroup;

    @Column(name = "description_group", nullable = false, length = 100)
    private String descriptionGroup;

    @Column(name = "expiration_date", nullable = false, length = 100)
    private LocalDate expirationDate;

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "groups_roles",
//            joinColumns = @JoinColumn(name = "id_group"),
//            inverseJoinColumns = @JoinColumn(name = "id_role"))
//    private List<Role> roles = new ArrayList<>();

    //GETTER, SETTER for Role
    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }
////////////////////////////////////////////////////////

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "persons_groups",
//            joinColumns = @JoinColumn(name = "id_person"),
//            inverseJoinColumns = @JoinColumn(name = "id_group")
//    )
//    private List<Person> persons = new ArrayList<>();

    @ManyToMany(mappedBy = "group", fetch = FetchType.LAZY)
    List<Person> person = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "groups_roles",
            joinColumns = @JoinColumn(name = "id_group"),
            inverseJoinColumns = @JoinColumn(name = "id_role"))
    @OrderBy("idRole asc")
    List<Role> role = new ArrayList<>();


//    @JoinTable(name = "groups_persons",
//            joinColumns = @JoinColumn(name = "id_group"),
//            inverseJoinColumns = @JoinColumn(name = "id_person"))
//    private Set<Role> role;


//    @ManyToMany(mappedBy = "groups")
//    private Set<Person> persons;
//    //GETTER, SETTER for Persons


    public List<Person> getPerson() {
        return person;
    }

    public void setPerson(List<Person> person) {
        this.person = person;
    }

    public Group() {
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    public String getDescriptionGroup() {
        return descriptionGroup;
    }

    public void setDescriptionGroup(String descriptionGroup) {
        this.descriptionGroup = descriptionGroup;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }


    @Override
    public String toString() {
        return "Group{" +
                "idGroup=" + idGroup +
                ", nameGroup='" + nameGroup + '\'' +
                ", descriptionGroup='" + descriptionGroup + '\'' +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
