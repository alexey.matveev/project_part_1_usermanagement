package com.cgi.usermanagement.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "role")
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_role")
    private int idRole;

    @Column(name = "role_type", nullable = false, length = 100)
    private String roleType;

    @Column(name = "description_role", nullable = false, length = 100)
    private String descriptionRole;

    public Role() {
    }

    @ManyToMany(mappedBy = "role", fetch = FetchType.LAZY)
    List<Group> group = new ArrayList<>();
    //GETTER, SETTER for Group

    public List<Group> getGroup() {
        return group;
    }

    public void setGroup(List<Group> group) {
        this.group = group;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescriptionRole() {
        return descriptionRole;
    }

    public void setDescriptionRole(String descriptionRole) {
        this.descriptionRole = descriptionRole;
    }

    @Override
    public String toString() {
        return "Role{" +
                "idRole=" + idRole +
                ", roleType='" + roleType + '\'' +
                ", descriptionRole='" + descriptionRole + '\'' +
                '}';
    }
}
