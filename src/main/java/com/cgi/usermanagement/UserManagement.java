package com.cgi.usermanagement;

import com.cgi.usermanagement.entities.Group;
import com.cgi.usermanagement.repository.PersonRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Optional;


@SpringBootApplication
public class UserManagement {

    public static void main(String[] args) {
        SpringApplication.run(UserManagement.class, args);
//        AnnotationConfigApplicationContext annotationConfigApplicationContext =
//				new AnnotationConfigApplicationContext(UserManagement.class);
//
//		PersonRepository personRepository = annotationConfigApplicationContext.getBean(PersonRepository.class);
//
//		Optional<Group> group =  personRepository.findGroup(1);
//
//		System.out.println("Group.get() : " + group.get() + "Group : " + group);
   }
}
