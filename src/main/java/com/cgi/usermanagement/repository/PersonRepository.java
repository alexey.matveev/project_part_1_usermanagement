package com.cgi.usermanagement.repository;


import com.cgi.usermanagement.entities.Group;
import com.cgi.usermanagement.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    //SELECT BY ID
    @Query("SELECT p FROM Person p WHERE p.idPerson = :id")
    Optional<Person> findPersonById(@Param("id") Long id);

    //SELECT BY EMAIL
    @Query("SELECT p FROM Person p WHERE p.email = :email")
    Optional<Person> findPersonByEmail(@Param("email") String email);

    //SELECT BY ID with Address, Group, Role info
   @Query("SELECT p FROM Person p JOIN FETCH p.address a WHERE p.idPerson = :id")
 //   @Query("SELECT p FROM Person p JOIN FETCH p. WHERE p.idPerson = :id")
    Optional<Person> findPersonByIdWithAllInfo(@Param("id") Long id);

// ////////////////////////////////////////////////////////////////////////////////////  !!!!!!!
//    //SELECT GROUP BY ID  //    //
    //test---------------------------------to delete!!!
//    @Query("SELECT p FROM Group p WHERE p.idGroup = :id")
//    Optional<Group> findGroup(@Param("id") int id);
//




}
