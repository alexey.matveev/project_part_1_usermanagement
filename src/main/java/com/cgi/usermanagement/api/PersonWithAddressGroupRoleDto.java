package com.cgi.usermanagement.api;

import java.util.List;

public class PersonWithAddressGroupRoleDto {

    private Long idPerson;
    private String name;
    private String email;
    //private String password;
    //private int salary;

    private PersonAddressDto address;

    private List<GroupDto> group;


    public PersonWithAddressGroupRoleDto() {
    }

    public PersonWithAddressGroupRoleDto(Long idPerson, String name, String email, PersonAddressDto address, List<GroupDto> group) {
        this.idPerson = idPerson;
        this.name = name;
        this.email = email;
        this.address = address;
        this.group = group;
    }

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PersonAddressDto getAddress() {
        return address;
    }

    public void setAddress(PersonAddressDto address) {
        this.address = address;
    }

    public List<GroupDto> getGroup() {
        return group;
    }

    public void setGroup(List<GroupDto> group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return "PersonWithAddressGroupRoleDto{" +
                "idPerson=" + idPerson +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", group=" + group +
                '}';
    }
}
