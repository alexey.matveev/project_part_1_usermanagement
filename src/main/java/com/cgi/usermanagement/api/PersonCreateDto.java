package com.cgi.usermanagement.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class PersonCreateDto {

    private Long idPerson;
    @NotEmpty(message = "It's requried to input Firstname")
    private String name;
    @Email
    private String email;
    @Size(min = 5, max = 45)
  //    @JsonIgnore
    private String password;
    private int salary;

    public Long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(Long idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public PersonCreateDto() {
    }

    @Override
    public String toString() {
        return "PersonCreateDto{" +
                "idPerson=" + idPerson +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", salary=" + salary +
                '}';
    }
}
