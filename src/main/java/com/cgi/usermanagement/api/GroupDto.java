package com.cgi.usermanagement.api;


import java.time.LocalDate;
import java.util.List;

public class GroupDto {
    private int idGroup;
    private String nameGroup;
    private String descriptionGroup;
    private LocalDate expirationDate;

    private List<RoleDto> role;

    public GroupDto() {
    }

    public GroupDto(int idGroup, String nameGroup, String descriptionGroup, LocalDate expirationDate, List<RoleDto> role) {
        this.idGroup = idGroup;
        this.nameGroup = nameGroup;
        this.descriptionGroup = descriptionGroup;
        this.expirationDate = expirationDate;
        this.role = role;
    }

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getNameGroup() {
        return nameGroup;
    }

    public void setNameGroup(String nameGroup) {
        this.nameGroup = nameGroup;
    }

    public String getDescriptionGroup() {
        return descriptionGroup;
    }

    public void setDescriptionGroup(String descriptionGroup) {
        this.descriptionGroup = descriptionGroup;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public List<RoleDto> getRole() {
        return role;
    }

    public void setRole(List<RoleDto> role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "GroupDto{" +
                "idGroup=" + idGroup +
                ", nameGroup='" + nameGroup + '\'' +
                ", descriptionGroup='" + descriptionGroup + '\'' +
                ", expirationDate=" + expirationDate +
                ", role=" + role +
                '}';
    }
}
