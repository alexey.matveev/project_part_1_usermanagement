package com.cgi.usermanagement.api;

public class RoleDto{
    private int idRole;
    private String roleType;
    private String descriptionRole;

    public RoleDto() {
    }

    public RoleDto(int idRole, String roleType, String descriptionRole) {
        this.idRole = idRole;
        this.roleType = roleType;
        this.descriptionRole = descriptionRole;
    }

    public int getIdRole() {
        return idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getDescriptionRole() {
        return descriptionRole;
    }

    public void setDescriptionRole(String descriptionRole) {
        this.descriptionRole = descriptionRole;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "idRole=" + idRole +
                ", roleType='" + roleType + '\'' +
                ", descriptionRole='" + descriptionRole + '\'' +
                '}';
    }
}
