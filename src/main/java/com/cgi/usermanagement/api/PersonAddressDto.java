package com.cgi.usermanagement.api;

public class PersonAddressDto {

    private Long idAddress;
    private String city;
    private String country;
    private String street;
    private String postCode;

    public PersonAddressDto() {
    }

    public PersonAddressDto(Long idAddress, String city, String country, String street, String postCode) {
        this.idAddress = idAddress;
        this.city = city;
        this.country = country;
        this.street = street;
        this.postCode = postCode;
    }

    public Long getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(Long idAddress) {
        this.idAddress = idAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
