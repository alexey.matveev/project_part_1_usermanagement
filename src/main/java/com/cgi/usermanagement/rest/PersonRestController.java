package com.cgi.usermanagement.rest;

import com.cgi.usermanagement.api.GroupDto;
import com.cgi.usermanagement.api.PersonCreateDto;
import com.cgi.usermanagement.api.PersonDto;
import com.cgi.usermanagement.api.PersonWithAddressGroupRoleDto;
import com.cgi.usermanagement.entities.Group;
import com.cgi.usermanagement.repository.PersonRepository;
import com.cgi.usermanagement.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;


@RestController
@RequestMapping(path = "/persons")
public class PersonRestController {
@Autowired
    private PersonRepository personRepository;
    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }


    //@GetMapping("/{id}")
    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> getPersonById(@PathVariable("id") Long id) {
        System.out.println("PersonRestController. Try to find Person by this ID:  "+ id);
        return ResponseEntity.ok(personService.findPersonById(id));
    }

    //@GetMapping("/{email}")
    @GetMapping("/email/{email}")
    public ResponseEntity<PersonDto> getPersonByEmail(@PathVariable("email") String email) {
        System.out.println("PersonRestController. Try to find Person by this email:  "+ email);
        return ResponseEntity.ok(personService.findPersonByEmail(email));
    }


    //@GetMapping("/{id}")
    @GetMapping("/allinfo/{id}")
    public ResponseEntity<PersonWithAddressGroupRoleDto> getPersonByIdWithAllInfo(@PathVariable("id") Long id) {
        System.out.println("PersonRestController. Try to find Person WITH FULL INFO by this ID:  " + id);

        return ResponseEntity.ok(personService.findPersonByIdWithAllInfo(id));
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PersonDto> createPerson(@Valid @RequestBody PersonCreateDto personCreateDto) {
        return ResponseEntity.ok(personService.createPerson(personCreateDto));

    }


}
